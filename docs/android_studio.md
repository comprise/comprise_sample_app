# Install Android Studio 

If not already installed, the installation of **Android Studio** is also recommended, since it contains **ADB**, a tool to make a connection to your Android device. 

Download Android Studio [here](https://developer.android.com/studio).

Afterwards, follow the installation instructions for your platform (Windows / MacOS / Linux):

- [Windows](https://developer.android.com/studio/install#windows)
- [MacOS](https://developer.android.com/studio/install#mac)
- [Linux](https://developer.android.com/studio/install#linux)

## Ubuntu

Within Ubuntu, running Android Studio for the first time will install the Android SDK. Then add this to your `.bash_profile`:

```bash
export ANDROID_SDK_ROOT=/home/evincent/Android/Sdk
export PATH=$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/tools/bin:$ANDROID_SDK_ROOT/build-tools:${PATH}
```

Then run:

```bash
yes | sdkmanager --licenses
sudo apt-get install gradle
```

You also need a version of **Android SDK** on your system. You should be able to download them using the **SDK Manager of Android Studio**. 

Any Android SDK **> API Level 26** has been tested so far. 