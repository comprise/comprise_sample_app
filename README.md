# COMPRISE Sample App

This Ionic (Angular) Application provides an example to on how to integrate COMPRISE-related component properly.

The usecase of this app simply is to transform native speech to textual pivot-language, interprete it and deliver suitable answer back, natively.

In addition, sensitive speech and text information shall be transformed.

<img src="./src/assets/img/screen.png" width="200" height="400">
 
## Prepare Environment
  

Setup your production environment as explained in the chapters below:

### For Android:

- [Setup a Smartphone](./docs/phone.md)
- [Install Android Studio](./docs/android_studio.md)
- [Install Java Development Kit 8](./docs/jdk.md)
- [Install NodeJS](./docs/nodejs.md)
- [Install Docker](./docs/docker.md) 

Note: Docker Installation is only needed if you setup your own COMPRISE Personal Server later. If you use trusted third party provider, you can skip.

### For iOS:

Will be added soon!

## Setup Personal Server

If you want to connect the app to an own instance of the COMPRISE Personal Server, find instructions [here](https://gitlab.inria.fr/comprise/comprise-personal-server) how to setup.

## Application configuaration with COMPRISE Application Wizard

If not done already, at latest by now, you need to clone this repository to your device.

```bash
git clone https://gitlab.inria.fr/comprise/comprise_sample_app.git
```

If(!) you are only planning on connect Server and App, but nothing else left to be done, you can also simply open `package.json` 
and add a parameter called `personalizedServerURL` with the URL the Personal Server generated.

If you want to to additional configuaration or just want to go the official way, continue with the instructions given [within COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard).

**Note:** 

## Use the COMPRISE Sample App 

Install all dependencies needed to your app by running the command below. 

```bash
npm run generate_android
```
 
It already generates an APK-File, to be found at `comprise_sample_app/platforms/android/app/build/outputs/apk/debug/app-debug.apk`

To run the app on your connected device run:

```bash
ionic cordova run android
```

If you are running on Android Studio and not within command line, open `comprise_sample_app/platforms/android` as project and execute "Run" command.

### Executable Scripts (package.json)

Executable with `npm run <script-tag>`

- `generate_android`: Generates a new, fresh Android-Application (with some example STT models assigned)
- `update_cordova_plugins`: Removes and updates COMPRISE-related Cordova-Plugins (currently STT only)

Of course, methods can be extended by developers themselves! Do not forget to update this documentation and commit / push recent changes.

### Structure 

The structure of the library itself (`projects/asc/x-core/`):

#### package.json

Includes every (dev-)dependencies needed in COMPRISE: 

**Possible NPM Plugins contacting [COMPRISE Personal Server](https://gitlab.inria.fr/comprise/comprise-personal-server):**
  
- [comprise_cloud_platform_api](https://www.npmjs.com/package/comprise_cloud_platform_api)
- [comprise_machine_translation](https://www.npmjs.com/package/comprise_machine_translation)
- [comprise_natural_language_generation](https://www.npmjs.com/package/comprise_natural_language_generation)
- [comprise_natural_language_understanding](https://www.npmjs.com/package/comprise_natural_language_understanding)
- [comprise_personalized_learning](https://www.npmjs.com/package/comprise_personalized_learning)
- [comprise_privacy_driven_speech_transformation](https://www.npmjs.com/package/comprise_privacy_driven_speech_transformation)
- [comprise_privacy_driven_text_transformation](https://www.npmjs.com/package/comprise_privacy_driven_text_transformation)
- [comprise_speech_to_text](https://www.npmjs.com/package/comprise_speech_to_text)
- [comprise_text_to_speech](https://www.npmjs.com/package/comprise_text_to_speech) 

To install a plugin, pick a name from the list above, like: `npm install --save comprise_cloud_platform_api@latest`

To uninstall that package again, execute :`npm uninstall --save comprise_cloud_platform_api`

**Possible Plugins being able to run locally:**

- [cordova-plugin-comprise-speech-to-text](https://www.npmjs.com/package/cordova-plugin-comprise-speech-to-text)
- [cordova-plugin-comprise-text-to-speech](https://www.npmjs.com/package/cordova-plugin-comprise-text-to-speech)
 

To install, pick a name from the list above, like `ionic cordova plugin add cordova-plugin-comprise-speech-to-text@latest`

To uninstall that package again, execute `ionic cordova plugin remove cordova-plugin-comprise-speech-to-text`
 
**Parameters attached by [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard):**

- `personalizedServerURL`: URL which will be used by all packages above to find the [COMPRISE Personal Server](https://gitlab.inria.fr/comprise/comprise-personal-server), 
	which executes the logic of your function call at the end

#### ./src/app/providers/privatestorage.ts

Contains globally needed parameters.

- `lang`: language in the format "xx-XX" which will be needed to access multiple language dependent services.
- `get langShort()`: gets language in the short format "xx".
- `client_id_mt`: Client credentials for [TILDE Machine Translation API](https://tilde.com/products-and-services/machine-translation), as this will be needed during MT.
- `yandex_id_mt`: Client credentials for [Yandex API](https://yandex.com/dev/translate/). Fallback solution (!) if TILDE MT does not support language pair.
- `ocpApimSubscriptionKey`: API Key for accessing [Intent Detection](https://dev-nlu1-am.portal.azure-api.net/).
- `cloudPlatfromAppID`: ID of the App in [Cloud Platform](https://comprise-dev.tilde.com/dashboard).
- `cloudPlatfromLanguage`: language of Cloud Platform in the format "xx-XX".
- `dmApiSecret`: API Key for [Dialog Management](https://botdashboard.tilde.ai/Conversations?id=KV6PrudaVqiELNxdTiuOLO-a&bot=65).
- `dmConversationId`: ID of conversation, if we want to use a specific conversation within Dialog Management.

#### ./src/app/home/home.component.ts

Contains the example code. All functionality to be included is documented in [COMPRISE Client Library](https://gitlab.inria.fr/comprise/comprise_client_library).

NPM plugins can be included as follows: 

```typescript
import { startRecording } from 'comprise_speech_to_text'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	startRecording().then(() => {
		console.log("Start talking to me until I stop");
	},(onerror) => {
		console.log("Following error happened :"+onerror);
	}); 
}

```

Cordova-Plugins can be included as follows: 

```typescript
// home.component.ts

import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
...

declare var cordova: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
	
	constructor(public plt: Platform) { 	
		this.plt.ready().then(() => {
			cordova.plugins.COMPRISE_SpeechToText.hasPermission((hasPermission) => {
				console.log(hasPermission)
			});
		});
	}
}

``` 