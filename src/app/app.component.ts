import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
	private translateService: TranslateService
	
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
		try {
		var xmlHttp = new XMLHttpRequest();
			xmlHttp.open('POST', "http://localhost/comprise_sdk/setloaded", true); // true for asynchronous
			xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlHttp.send(null);
		} catch(e) {}
		
		this.translateService.setDefaultLang('en');
        this.translateService.use('en');
		
		this.statusBar.styleDefault();
		this.splashScreen.hide();
    });
  }
}
