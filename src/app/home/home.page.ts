import { Component, NgZone } from '@angular/core';

import { transformPrivateSpeech } from 'comprise_privacy_driven_speech_transformation'
import { transformPrivateText, FULL_ENTITY_REPLACEMENT } from 'comprise_privacy_driven_text_transformation'

import { transformToSpeech } from 'comprise_text_to_speech'
import { startRecording, stopRecording } from 'comprise_speech_to_text'

import { translate } from 'comprise_machine_translation'
import { uploadSpeechData, uploadTextData } from 'comprise_cloud_platform_api'

import { detectIntent } from 'comprise_natural_language_understanding'
import { generateAnswerToMessage } from 'comprise_natural_language_generation'

import { Platform } from '@ionic/angular';
import { PrivateStorage } from '../providers/privatestorage';


import { TranslateService } from '@ngx-translate/core';
import * as pjson from '../../../package.json';
import { HTTP } from '@ionic-native/http/ngx';
import { Media } from '@ionic-native/media/ngx';
import { File } from '@ionic-native/file/ngx'; 

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import * as RecordRTC  from 'recordrtc';
declare var cordova: any;
declare var navigator: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage { 

	readyMic = false;
	isRecording = false;
	
	filePath : string;
	audio: any;
    recorder: any = null;

	constructor(public privateStorage: PrivateStorage, 
		public translateService: TranslateService,
		public androidPermissions: AndroidPermissions,
		public zone: NgZone, 
		public plt: Platform, 
		public http: HTTP, 
		public media: Media, 
		public file: File
	) {
		//this.privateStorage.lang = "en-EN"
		let env = this;
		env.plt.ready().then(() => {
			cordova.plugins.COMPRISE_SpeechToText.hasPermission((hasPermission) => { 
				if(hasPermission) {
					cordova.plugins.COMPRISE_SpeechToText.initialize((done) => {
						if(done) { 
							env.zone.run(()=>{
								env.readyMic = true;
							});
						}
					});
				} else {
					cordova.plugins.COMPRISE_SpeechToText.requestPermission(() => {
						cordova.plugins.COMPRISE_SpeechToText.initialize((done) => {
							if(done) { 
								env.zone.run(()=>{
									env.readyMic = true;	 
								});
							}
						});
					}, () => {
						console.log('Not authorized');
					});
				}
			});
		});
	}
	
	isMicReady() {
		return this.readyMic;
	}
	
	changeLanguage(event) {
		this.privateStorage.lang = event.target.value;
		this.translateService.setDefaultLang(this.privateStorage.langShort);
        this.translateService.use(this.privateStorage.langShort);
	} 
	 
	record2() {
		let env = this;
		if(!this.isRecording) {
			env.isRecording = true;
			cordova.plugins.COMPRISE_SpeechToText.startListening((sentence: string) => {
				env.isRecording = false;
				document.getElementById("speech_to_text").innerHTML = sentence;			
				env.translateToPivotLanguage(sentence);						
			}, (onerror) => {
				console.log('error:', onerror)
			}); 
		} else {
			env.isRecording = false;
			cordova.plugins.COMPRISE_SpeechToText.stopListening((sentence: string) => {
				document.getElementById("speech_to_text").innerHTML = sentence;			
				env.translateToPivotLanguage(sentence);						
			}, (onerror) => {
				console.log('error:', onerror)
			}); 
		}
	}
	
	
	record() {
		const env = this;	
		env.androidPermissions.requestPermissions([
			env.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS,
            env.androidPermissions.PERMISSION.RECORD_AUDIO
		]).then(()=>{
            if(!env.isRecording) {
				startRecording().then(() => {
					env.isRecording = true;
				});
			} else {
                stopRecording(this.privateStorage.langShort, env.http, false).then((text) => {
					document.getElementById("speech_to_text").innerHTML = text;
					env.isRecording = false;
					env.transmitTextToCloud(text);
				},(onerror2) => {
					env.isRecording = false;
					console.log(onerror2)
				});  
			}
		})
	}
	
	transmitTextToCloud(text: string) {
		let env = this;
		transformPrivateText(text, "WORD", env.http).then((transformedText) => {
			document.getElementById("privacy_driven_text_transformation").innerHTML = transformedText;
			
			uploadTextData(text, env.http).then((res) => {
				if(res) {
					env.transmitSpeechToCloud(text);
					console.log("Text uploaded.")
				} else {
					env.isRecording = false;
					console.log("Text not uploaded.")
				};
			},(textUploadError) => {
				env.isRecording = false;
				console.log("Text not uploaded.",textUploadError)
			});
			
		},(textTransformationError) => {
			env.isRecording = false;
			console.log(textTransformationError)
		});
	}
	
	_base64ToArrayBuffer(base64) {
		var binary_string = window.atob(base64);
		var len = binary_string.length;
		var bytes = new Uint8Array(len);
		for (var i = 0; i < len; i++) {
			bytes[i] = binary_string.charCodeAt(i);
		}
		return bytes.buffer;
	}
	
	transmitSpeechToCloud(transformedText: string) {
		let env = this;
		transformPrivateSpeech(env.http).then((sound) => {
			console.log(sound)
			const audio = new Audio();
			audio.src = "data:audio/wav;base64,"+sound;
			audio.play();
			
			var buffer = env._base64ToArrayBuffer(sound)
			console.log(buffer);
			
			uploadSpeechData(buffer, env.http).then((res) => {
				if(res) {
					console.log("Speech uploaded.")
					env.translateToPivotLanguage(transformedText);
				} else {
					console.log("Speech not uploaded.")
				};
			},(speechUploadError) => {
				console.log("Speech not uploaded.", onerror)
			});
		},(speechTransformationError) => {
			console.log("Speech not uploaded.", onerror)
		});
	}
	
	translateToPivotLanguage(sentence) {
		const env = this;
		var data = null; 
		
		translate(sentence, this.privateStorage.langShort, "en", this.privateStorage.client_id_mt, this.http).then((translation) => {
			document.getElementById("machine_translation").innerHTML = translation;
			env.nlu(translation);
		},(onerror) => {
			console.log(onerror)
		});
	}
	
	nlu(translation: string) {
		const env = this;
		detectIntent(translation, this.privateStorage.cloudPlatfromAppID, this.privateStorage.cloudPlatfromLanguage, 
			this.privateStorage.ocpApimSubscriptionKey, this.http).then((intent) => {
			document.getElementById("intent").innerHTML = intent;
			env.nlg(intent);
		},(onerror) => {
			console.log(onerror)
		});
	}
	 
	nlg(intent: string) {
		const env = this;
		generateAnswerToMessage(this.privateStorage.dmConversationId, intent, this.privateStorage.dmApiSecret ,this.http).then((answerObj) => { 
			let answer = answerObj.answer
			console.log(answer);
			document.getElementById("answer").innerHTML = answer;
			this.privateStorage.dmConversationId = answerObj.conversationId;
			env.translateToNative(answer);
		}).catch((error) => {
			console.log(error);
		});
	}
	
	translateToNative(sentence) {
		const env = this;
		translate(sentence, "en", this.privateStorage.langShort, this.privateStorage.client_id_mt, this.http).then((translation) => {
			document.getElementById("translated_answer").innerHTML = translation;
			env.tts(translation);
		}, (error) => {
			console.log("MT Error: ",error);
		});
	}
	
	tts(sentence: string) { 
		transformToSpeech(sentence, this.privateStorage.langShort, this.http).then((sound) => {
			console.log('Success:', sound) 
		}, (error) => {
			console.log("TTS Error: ",error);
		});
	}

}
