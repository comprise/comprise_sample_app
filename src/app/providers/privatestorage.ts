import { Inject, Injectable, NgZone, OnInit } from '@angular/core';
import * as pjson from '../../../package.json';

@Injectable({
  providedIn: 'root',
})
export class PrivateStorage {
	lang: string = ""; 
	supportedLanguages: any = [];
 
	get langShort(): any {
        return this.lang.split("-")[0];
    }
	
	constructor() { 
		this.supportedLanguages = pjson['supportedLanguages'].split(",");
		this.lang = "en-US";
	}
	
	client_id_mt = "u-b583340b-e18b-4a88-920f-4b2f1d199a06";
	
	ocpApimSubscriptionKey = "b0b68a32871846c587fed8e202b9020c";
	cloudPlatfromAppID = "prodbotcompris637944618";
	cloudPlatfromLanguage = "en-en";
	
	dmConversationId = null; //"C8Im1M0g4Vp2dlnbEmEH3f-m";
	dmApiSecret = "KEVSSGrBAbM.aHdn6XIpOwJ2XbdOujgDqLZ-9ndnQkZNuiUmZ9Vf_ro";
 
}