export const environment = {
  production: true,
  defaultLanguage: 'en-US',
  supportedLanguages: ['en-US', 'de-DE', 'es-ES', 'fr-FR', 'lv-LV']
};

